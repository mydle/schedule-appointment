json.array!(@patients) do |patient|
  json.extract! patient, :id, :name, :phone, :email, :insurance_id
  json.url patient_url(patient, format: :json)
end
