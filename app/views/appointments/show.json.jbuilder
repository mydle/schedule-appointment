json.extract! @appointment, :id, :appointment_date, :time_slot_id, :patient_id, :reason, :status_id, :physician_id, :diagnostic_id, :note, :fee, :created_at, :updated_at
