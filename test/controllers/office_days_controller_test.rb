require 'test_helper'

class OfficeDaysControllerTest < ActionController::TestCase
  setup do
    @office_day = office_days(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:office_days)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create office_day" do
    assert_difference('OfficeDay.count') do
      post :create, office_day: { work_day: @office_day.work_day }
    end

    assert_redirected_to office_day_path(assigns(:office_day))
  end

  test "should show office_day" do
    get :show, id: @office_day
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @office_day
    assert_response :success
  end

  test "should update office_day" do
    patch :update, id: @office_day, office_day: { work_day: @office_day.work_day }
    assert_redirected_to office_day_path(assigns(:office_day))
  end

  test "should destroy office_day" do
    assert_difference('OfficeDay.count', -1) do
      delete :destroy, id: @office_day
    end

    assert_redirected_to office_days_path
  end
end
