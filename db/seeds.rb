# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# insurance data
insurances = Insurance.create ([
    {name: 'Humana', toll_free: '1-800-HUMANA'},
    {name: 'Farmer', toll_free: '1-800-FARMER'},
    {name: 'All State', toll_free: '1-800-ALL-STATE'},
    {name: 'Geico', toll_free: '1-800-GEICO'},
    {name: 'Liberty', toll_free: '1-800-LIBERTY'}

])

# patient data
patients = Patient.create ([
    {name: 'Cherry', phone: '111-111-1111', email:'cherry@aol.com' , insurance_id: 1},
    {name: 'Berry', phone: '222-222-2222', email:'berry@yahoo.com', insurance_id: 2},
    {name: 'Apple', phone: '333-333-3333',email:'apple@hotmail.com', insurance_id: 3},
    {name: 'Watermelon', phone: '444-444-4444',email:'watermelon@gmail.com', insurance_id: 4},
    {name: 'Grapefruit', phone: '555-555-5555',email:'grapefruit@att.net', insurance_id: 5}
])

# physician data
physicians = Physician.create([
                                    {name: 'Bush'},
                                    {name: 'Washington'},
                                    {name: 'Jennifer'},
                                    {name: 'Ford'},
                                    {name: 'Miller'}

                                ])

# office_day data
office_days = OfficeDay.create ([
    {work_day: 'Monday'},
    {work_day: 'Tuesday'},
    {work_day: 'Wednesday'},
    {work_day: 'Thursday'},
    {work_day: 'Friday'}
])

# time_slots data
time_slots = TimeSlot.create ([
    {start_at:'8:00 AM', end_at:'8:30 AM'},
    {start_at:'8:30 AM', end_at:'9:00 AM'},
    {start_at:'9:00 AM', end_at:'9:30 AM'},
    {start_at:'9:30 AM', end_at:'10:00 AM'},
    {start_at:'10:00 AM', end_at:'10:30 AM'},
    {start_at:'10:30 AM', end_at:'11:00 AM'},
    {start_at:'11:00 AM', end_at:'11:30 AM'},
    {start_at:'11:30 AM', end_at:'12:00 PM'},
    {start_at:'12:00 PM', end_at:'12:30 PM'},
    {start_at:'12:30 PM', end_at:'1:00 PM'},
    {start_at:'1:00 PM', end_at:'1:30 PM'},
    {start_at:'1:30 PM', end_at:'2:00 PM'},
    {start_at:'2:00 PM', end_at:'2:30 PM'},
    {start_at:'2:30 PM', end_at:'3:00 PM'},
    {start_at:'3:00 PM', end_at:'3:30 PM'},
    {start_at:'3:30 PM', end_at:'4:00 PM'},
    {start_at:'4:00 PM', end_at:'4:30 PM'}

])

# status data
statuses = Status.create ([
    {appointment_status: 'Available'},
    {appointment_status: 'Filled'},
    {appointment_status: 'Cancel'},
])

# appointment data
appointments = Appointment.create ([
    {appointment_date:'2014-12-03',time_slot_id: 1, office_day_id: 3, patient_id: 1, reason:'Need acne treatment', status_id: 2, physician_id: 1,diagnostic_id: 527, note: '', fee:'25.00' },
    {appointment_date:'2014-12-04',time_slot_id: 3, office_day_id: 4, patient_id: 2, reason:'Inflammation of skin', status_id: 2, physician_id: 2,diagnostic_id: 576, note: 'Topical corticosteroid creams and ointments. Put on the place that have redness, itchy. Use twice a day', fee:'45.00' },
    {appointment_date:'2014-12-05',time_slot_id: 5, office_day_id: 5, patient_id: 3, reason:'Skin burn', status_id: 3, physician_id: 3,diagnostic_id: 550, note: '', fee:'30.00' },
    {appointment_date:'2014-12-08',time_slot_id: 6, office_day_id: 1, patient_id: 4, reason:'Unknown', status_id: 2, physician_id: 4,diagnostic_id: 525, note: '', fee:'50.00' },
    {appointment_date:'2014-12-10',time_slot_id: 7, office_day_id: 3, patient_id: 5, reason:'N/A', status_id: 3, physician_id: 5,diagnostic_id: 540, note: '', fee:'35.00' }


])

#load Diagnostic code data
require 'csv'
CSV.foreach("#{Rails.root}/lib/tasks/data.csv") do |row|
  params =[:code => row]
  Diagnostic.create! (Hash[:code => row[0]])
end

