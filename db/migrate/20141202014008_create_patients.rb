class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.integer :insurance_id

      t.timestamps
    end
  end
end
